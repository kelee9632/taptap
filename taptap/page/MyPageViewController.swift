//
//  PageViewController.swift
//  taptap
//
//  Created by uniwiz on 2023/03/17.
//

import UIKit
import RxSwift
import RxCocoa


class MyPageViewController: UIViewController {
    @IBOutlet weak var imageButton: UIButton!
    @IBOutlet weak var pageControllerStack: UIStackView!
    @IBOutlet weak var firstDot: UIImageView!
    @IBOutlet weak var secondDot: UIImageView!
    @IBOutlet weak var thirdDot: UIImageView!
    @IBOutlet weak var forthDot: UIImageView!
    
    var myIndex = 0
    let publishSubject = PublishSubject<String>()
    var orientation = ""
    
    lazy var vc1:UIViewController = {
        let vc = UIViewController()
        let myImage = UIImage(named:"img01")
        let imageView = UIImageView()
        imageView.image = myImage
        imageView.contentMode = .scaleAspectFit
        vc.view.insertSubview(imageView, at: 0)
        imageConfigure(image: imageView,vc:vc)
        return vc
    }()
    
    lazy var vc2:UIViewController = {
        let vc = UIViewController()
        let myImage = UIImage(named:"img02")
        let imageView = UIImageView()
        imageView.image = myImage
        imageView.contentMode = .scaleAspectFit
        vc.view.insertSubview(imageView, at: 0)
        imageConfigure(image: imageView,vc:vc)
        return vc
    }()
    
    lazy var vc3:UIViewController = {
        let vc = UIViewController()
        let myImage = UIImage(named:"img03")
        let imageView = UIImageView()
        imageView.image = myImage
        imageView.contentMode = .scaleAspectFit
        vc.view.insertSubview(imageView, at: 0)
        imageConfigure(image: imageView,vc:vc)
        return vc
    }()
    lazy var vc4:UIViewController = {
        let vc = UIViewController()
        let myImage = UIImage(named:"img04")
        let imageView = UIImageView()
        imageView.image = myImage
        imageView.contentMode = .scaleAspectFit
        vc.view.insertSubview(imageView, at: 0)
        imageConfigure(image: imageView,vc:vc)
        return vc
    }()
    lazy var largeVc1:UIViewController = {
        let vc = UIViewController()
        let myImage = UIImage(named:"on1")
        let imageView = UIImageView()
        imageView.image = myImage
        imageView.contentMode = .scaleAspectFit
        vc.view.insertSubview(imageView, at: 0)
        imageConfigure(image: imageView,vc:vc)
        return vc
    }()
    lazy var largeVc2:UIViewController = {
        let vc = UIViewController()
        let myImage = UIImage(named:"on2")
        let imageView = UIImageView()
        imageView.image = myImage
        imageView.contentMode = .scaleAspectFit
        vc.view.insertSubview(imageView, at: 0)
        imageConfigure(image: imageView,vc:vc)
        return vc
    }()
    lazy var largeVc3:UIViewController = {
        let vc = UIViewController()
        let myImage = UIImage(named:"on3")
        let imageView = UIImageView()
        imageView.image = myImage
        imageView.contentMode = .scaleAspectFit
        vc.view.insertSubview(imageView, at: 0)
        imageConfigure(image: imageView,vc:vc)
        return vc
    }()
    lazy var largeVc4:UIViewController = {
        let vc = UIViewController()
        let myImage = UIImage(named:"on4")
        let imageView = UIImageView()
        imageView.image = myImage
        imageView.contentMode = .scaleAspectFit
        vc.view.insertSubview(imageView, at: 0)
        imageConfigure(image: imageView,vc:vc)
        return vc
    }()
    
    lazy var largeDataViewControllers: [UIViewController] = {
        return [largeVc1,largeVc2,largeVc3,largeVc4]
    }()
    
    lazy var dataViewControllers: [UIViewController] = {
        return [vc1, vc2, vc3, vc4]
    }()
    
    lazy var pageViewController: UIPageViewController = {
        let vc = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal,options: nil)
        
        return vc
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addObservers()
        configure()
        setUpDelegate()
        
        imageButton.setImage(UIImage(named: "btn01"),for:.normal)
        
        let myColor = hexStringToUIColor(hex: "#2733b2")
        view.backgroundColor = myColor
        
        getSize().subscribe{event in
            if(event){
                self.pageViewController.setViewControllers( [self.largeDataViewControllers[self.myIndex]], direction: .forward, animated: true,completion: nil)
                self.publishSubject.onNext("horizontal")
            } else{
                self.pageViewController.setViewControllers([self.dataViewControllers[self.myIndex]], direction: .forward, animated: true,completion: nil)
                self.publishSubject.onNext("vertical")
            }
        }
    }
    
    deinit{
        removeObervers()
    }
    
    private func configure(){
        addChild(pageViewController)
        view.addSubview(pageViewController.view)
        
        pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        pageViewController.view.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        pageViewController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        pageViewController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        pageViewController.view.bottomAnchor.constraint(equalTo: pageControllerStack.topAnchor).isActive = true
        
        pageViewController.didMove(toParent: self)
    }
    
    private func imageConfigure(image:UIImageView,vc:UIViewController){
        image.translatesAutoresizingMaskIntoConstraints = false
        image.topAnchor.constraint(equalTo: vc.view.safeAreaLayoutGuide.topAnchor).isActive = true
        image.trailingAnchor.constraint(equalTo: vc.view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        image.leadingAnchor.constraint(equalTo: vc.view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        image.bottomAnchor.constraint(equalTo: vc.view.bottomAnchor).isActive = true
    }
    
    private func setUpDelegate(){
        pageViewController.dataSource = self
        pageViewController.delegate = self
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func addObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.detectOrientation), name: NSNotification.Name("UIDeviceOrientationDidChangeNotification"), object: nil)
    }
    
    func removeObervers(){
        NotificationCenter.default.removeObserver(NSNotification.Name("UIDeviceOrientationDidChangeNotification"))
    }
    
    @objc func detectOrientation(){
        if(UIDevice.current.orientation == .landscapeLeft) || (UIDevice.current.orientation == .landscapeRight){
            pageViewController.setViewControllers( [largeDataViewControllers[myIndex]], direction: .forward, animated: true,completion: nil)
            publishSubject.onNext("horizontal")
        } else if (UIDevice.current.orientation == .portrait) || (UIDevice.current.orientation == .portraitUpsideDown){
            pageViewController.setViewControllers([dataViewControllers[myIndex]], direction: .forward, animated: true,completion: nil)
            publishSubject.onNext("vertical")
        }
    }
    
    func landScape() -> Bool{
        return UIApplication.shared.statusBarOrientation.isLandscape
    }
    
    func getSize() -> Observable<Bool>{
        Observable<Bool>.create({ (observer) -> Disposable in
            observer.onNext(UIApplication.shared.statusBarOrientation.isLandscape)
            observer.onCompleted()
            return Disposables.create()
        }
        )
    }
    
    func pageControl(pageIndex:Int){
        
        let colorDotImage = UIImage(named: "color_dot")
        let dotImage = UIImage(named: "dot"
        )
        
        print(pageIndex)
        switch(pageIndex){
        case 0:
            imageShow(0)
        case 1:
            imageShow(1)
        case 2:
            imageShow(2)
        case 3:
            imageShow(3)
        default:
            print("잘못된 신호입니다.")
        }
    }
    
    func imageShow( _ number:Int){
        let colorDotImage = UIImage(named: "color_dot")
        let dotImage = UIImage(named: "dot")
        
        if(number == 0){
            firstDot.image = colorDotImage
            secondDot.image = dotImage
            thirdDot.image = dotImage
            forthDot.image = dotImage
        } else if (number == 1){
            firstDot.image = dotImage
            secondDot.image = colorDotImage
            thirdDot.image = dotImage
            forthDot.image = dotImage
        }else if (number == 2){
            firstDot.image = dotImage
            secondDot.image = dotImage
            thirdDot.image = colorDotImage
            forthDot.image = dotImage
        }else{
            firstDot.image = dotImage
            secondDot.image = dotImage
            thirdDot.image = dotImage
            forthDot.image = colorDotImage
        }
    }
}


extension MyPageViewController:UIPageViewControllerDataSource, UIPageViewControllerDelegate{
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        publishSubject.subscribe(onNext:{ str in
            self.orientation = str
        })
        
        
        if(orientation == "vertical") {
            let previousIndex = myIndex - 1
            
            if previousIndex < 0 {
                return nil
            }
            return dataViewControllers[previousIndex]
        }else{
            let previousIndex = myIndex - 1
            
            if previousIndex < 0 {
                return nil
            }
            print("previous" + String(previousIndex))
            return largeDataViewControllers[previousIndex]
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        publishSubject.subscribe(onNext:{ str in
            self.orientation = str
        })
        
        if(orientation == "vertical") {
            let nextIndex = myIndex + 1
            if nextIndex == dataViewControllers.count {
                return nil
            }
            return dataViewControllers[nextIndex]
        } else {
            let nextIndex = myIndex + 1
            if nextIndex == largeDataViewControllers.count {
                return nil
            }
            return largeDataViewControllers[nextIndex]
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        if pendingViewControllers[0] == vc1{
            setBack(0, "#2733b2", "btn01")
        } else if pendingViewControllers[0] == vc2{
            setBack(1)
        }else if pendingViewControllers[0] == vc3{
            setBack(2)
        }else if pendingViewControllers[0] == vc4{
            setBack(3)
        } else if pendingViewControllers[0] == largeVc1{
            setBack(0, "#2733b2", "btn01")
        } else if pendingViewControllers[0] == largeVc2{
            setBack(1)
        }else if pendingViewControllers[0] == largeVc3{
            setBack(2)
        }else if pendingViewControllers[0] == largeVc4{
            setBack(3)
        }
        pageControl(pageIndex: myIndex)
        
    }
    
    func setBack(_ index:Int,_ color:String = "#fdfdfd",_ image:String = "btn02" ){
        myIndex = index
        let color = hexStringToUIColor(hex: color)
        view.backgroundColor = color
        imageButton.setImage(UIImage(named: image),for:.normal)
    }
}

