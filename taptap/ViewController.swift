//
//  ViewController.swift
//  taptap
//
//  Created by uniwiz on 2023/03/17.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let mypageViewController = MyPageViewController(nibName: "MyPageViewController", bundle: nil)
        mypageViewController.modalPresentationStyle = .overFullScreen
        mypageViewController.modalTransitionStyle = .crossDissolve
        self.present(mypageViewController,animated: true,completion: nil)
       
    }

}

